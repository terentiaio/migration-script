# Process For Creating An Embeddable Python That Can Run the Migration Script

Current as of: August 18, 2021

The problem being solved:

- Embeddable Python (see link below) is a bare-bones Python distribution with limited shared libraries that is meant to run without requiring install.
- The migration script requires several shared libraries
- This document will run you through how to add those required shared libraries.

## Windows

### Resources

Embeddable Python (64-bit):
https://www.python.org/downloads/windows/

Stack Overflow:
https://stackoverflow.com/questions/37710205/python-embeddable-zip-install-tkinter

### Steps

These steps assume that you have Python already installed on your machine and have pip installed all required dependencies.
I'm not listing the dependencies because they are in flux.
This is what I did, it's probably wrong, but it worked.

1. Download and extract embeddable python to a directory (e.g. /embeddable)
2. Copy main_gui.py and vars_to_import.py to /embeddable
3. Copy files in /modules (our local functions) to /embeddable/modules
4. Go to your installed Python directory (maybe c:/Python39)
5. Copy c:/Python39/tcl/ to /embeddable
6. Copy c:/Python39/Lib/tkinter/ to /embeddable
7. Copy c:/Python39/DLLs/_tkinter.pyd + tcl86t.dll + tk86t.dll to /embeddable
8. Take a break and grab some tea or coffee or beer or something that makes you feel really good
9. Navigate to /embeddable in a terminal and run `./python.exe main_gui.py`
10. There will be an error re: missing dependency
11. Open up File Explorer and search for that missing dependency. It will be a folder or file somewhere in the c:/Python39 directory (or a subdirectory)
12. Copy that folder or file into /embeddable
13. Go back to Step 9 and repeat until main_gui.py actually runs
14. Pat yourself on the back, you're a champion

## Mac

No idea yet.
