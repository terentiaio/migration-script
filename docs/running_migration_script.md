# Instructions for Running this Script

## Program Details

This migration script allows you to bulk ingest files on a local drive into Terentia. It will scan a directory for a) a properly-formatted .csv file describing the assets and b) the asset files described in a).

Components:

  1) The main GUI window, where you will enter the details of the ingest
  2) The status window, which will show you the progress of the ingest once it has started
  3) Console window, where further details about the ingest can be found, including any errors.

## Running the Program

1. Extract "migration_script.zip"
2. Open the extracted directory
3. Right-click on "migration_script.bat" and select "Run as Administrator"
4. Click Yes to confirm that you want to run this as an administrator.
5. A command prompt window will pop up followed by two GUI windows
6. Rearrange the two GUI windows so that both are visible. One window contains a progress bar for measuring the ingest process and the other contains the form fields where data related to the ingest is entered.

## Filling Out the Form Fields

1. Enter Host URL

  - this is the URL of the Terentia Server (API), NOT the UI or Admin Tool

2. Login

  - Username and Password for the Terentia Account this ingest should use.
  - Please ensure that it has proper security properties and/or access rights.

3. Select the Ingest Profile

4. Select the folder on your file system that contains the files to be ingested and the .csv file

  - the .csv file containing the asset and metadata data must be in the same folder as the files

5. Watch the status window to track the progress of the file ingest. More detailed information related to the ingest can be found in the command prompt window.
