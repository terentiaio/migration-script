# How to Create a Package You Can Send to Other People

## Instructions

1. Create a folder called migration_script
2. Copy embeddable/ into /migration_script/
3. Copy main_gui and vars_to_import into migration_script/embeddable/
4. Copy contents of /modules into migration_script/embeddable/modules/
5. Copy starter_scripts/migration_script.bat into migration_script/
6. Compress the whole thing