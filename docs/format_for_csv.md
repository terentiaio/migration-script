# CSV Format Standard

## Info/Notes

## Standard

Row 1 - Field names (organizational)
Row 2 - Field names in Terentia that map to Field names (organizational)
Row 3 - Field input type
Row 4 - Whether Field is mandatory or not

### Row 1

These can be anything. They represent organizational standards.

### Row 2

These must conform to the names of Metadata Types in Terentia or be first-class properties of the asset.

Valid first-class properties:

 - "Asset Title"
 - "Short Description"
 - "Long Description"

Metadata Type columns will have metadata records created for them.
First-class property columns will have those values added to the asset record.

### Row 3

These must conform to the names of input_types in Terentia.

- text
- email
- url
- number
- tel
- date
- datetime-local
- time
- week
- month
- select
- multi-select


### Row 4

"Yes" = mandatory
"No" = not mandatory

If a Field is labelled as mandatory and a file record is missing that data, it will fail to ingest.
