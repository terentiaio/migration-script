# Imports
from tkinter import *
from tkinter import filedialog, messagebox, ttk
from modules.feathers_api_calls import get_session_var, create_records, get_asset_class, login, get_ingest_profiles, get_upload_access_provider, check_metadata
from modules.csv_script import csv_script, json_logger, search_directory
from modules.upload_to_azure import uploadToAzure
from modules.reports_logger import initialize_and_start_logger, write_custom_line, write_success_line, write_failure_line, close_file_end_logger
import vars_to_import
import os


# Create root.
padding = 10
root = Tk()
root.title("Terentia - Client Asset Migration")
root.geometry(f"800x400+340+20")

top = Toplevel()
# Close the main window if the top is closed.
top.protocol("WM_DELETE_WINDOW", root.destroy)
top.title("Status Window")
top.geometry("300x75+20+20")
label_top_status_debug = Label(top, text="This is the status window.")
label_top_status_debug.pack()
label_top_counter = Label(top, text="Waiting for user entries...")
label_top_counter.pack()
progress_processed_assets = ttk.Progressbar(
    top, orient=HORIZONTAL, length=250, mode="determinate")
progress_processed_assets.pack()


# Var definitions.
current_dir = os.getcwd()
path_of_current_asset_file = None
user_id = None
upload_ap_data = None
upload_ap_container_name = None
upload_ap_account_name = None
upload_ap_key = None
total_number_of_valid_records = None
# Part 1 - Ingest Profile
ingest_profiles_raw_info = []
ingest_profiles_names_for_options = {}
options_selected_ingest_profile = StringVar()
options_ingest_profiles = None
selected_ingest_profile = None

# Part 2 - Folder path and file paths
folder_path = None
csv_file = None
asset_files = None
# Part 3 - Parsed csv
parsed_json_data = None


# Function definitions.

# Displays the login error label with the message.
# (message) - String with the message to display.
def show_login_error_label(message):
    label_login_error.config(text=message)
    label_login_error.grid()


# Hides the login error label.
def hide_login_error_label():
    label_login_error.grid_remove()

# Changes the login status according to the provided info.
# (message) - String with the message to display.
# (success) - Boolean. True = login success, False = login fail.


def edit_login_status_label(message, success):
    if success == True:
        label_login_status.config(text=message)
        entry_username.configure(state=DISABLED)
        entry_password.configure(state=DISABLED)
        button_login.configure(state=DISABLED)
    elif success == False:
        label_login_status.config(text=message, bg="#FF0000")


# This saves the host that the user has entered.
# If the user clicks Ok and continues,
# it removes the host related inputs
# and shows the login related inputs.
def confirm_host():
  try:
    user_input = entry_host_url.get()
    print(user_input[:7])
    if (user_input[:7] != "http://" and user_input[:8] != "https://"):
      user_input = "https://" + user_input
    box_response_host = messagebox.askokcancel("Host URL Confirm", "You have entered the host URL: '" + user_input + "' Press Ok if this host is correct and you want to continue. Press Cancel if you need to change your input.")
    if box_response_host == True:
        vars_to_import.host_url = user_input
        label_host_entry_instruction.grid_remove()
        label_host_entry_instruction_2.grid_remove()
        entry_host_url.grid_remove()
        button_confirm_host.grid_remove()

        label_host_status.config(
            text="1) Host was set to: " + vars_to_import.host_url)
        label_host_status.grid(row=1, column=0, sticky="nw", padx=padding, pady=(0, padding))
        label_login_status.grid()
        label_username_entry.grid()
        entry_username.grid()
        label_password_entry.grid()
        entry_password.grid()
        button_login.grid()

        # Log
        write_custom_line(f"Host set to {user_input}")
  except Exception as error:
    # Log
    write_custom_line("INGEST FAILED")
    write_custom_line(error)
    print("INGEST FAILED")
    print(error)


def attempt_login():
  try:
    global user_id
    hide_login_error_label()
    user_email = entry_username.get().strip()
    user_password = entry_password.get().strip()

    if len(user_email) == 0 or len(user_password) == 0:
        show_login_error_label("Email or password is null.")
    else:
        res_login = login(user_email, user_password)
        # print(json.dumps(res_login, indent=4, sort_keys=True))
        if res_login["status"] == "failedNoMessage":
            print("attempt_login res_login has returned with status: failedNoMessage")
            edit_login_status_label(
                "2) Login Status: Failure with no message.", False)
        elif res_login["status"] == "failedWithMessage":
            print("attempt_login res_login has returned with status: failedWithMessage")
            edit_login_status_label(
                "2) Login Status: Failure with error message. Message: " + res_login["data"]["message"] + ". Error code: " + str(res_login["data"]["code"]), False)
        elif res_login["status"] == "success":
            print("attempt_login res_login has returned with status: success")
            edit_login_status_label("2) Login Status: Success.", True)
            user_id = res_login["data"]["user"]["id"]
            get_upload_ap_data()
            get_and_setup_ingest_profiles()

            # Log
            write_custom_line(f"Logged in as user {user_email}")
  except Exception as error:
    # Log
    write_custom_line("INGEST FAILED")
    write_custom_line(error)
    print("INGEST FAILED")
    print(error)

def get_and_setup_ingest_profiles():
    global ingest_profiles_raw_info
    global ingest_profiles_names_for_options
    global options_selected_ingest_profile
    global options_ingest_profiles

    try:
      ingest_profiles = get_ingest_profiles()
      print("Getting ingest profiles")
      for ip in ingest_profiles["data"]:
          ingest_profiles_raw_info.append(ip)
          # Format a dict where the key is the IP title, and the value is the ID
          ingest_profiles_names_for_options[ip["name"]] = ip["id"]
          options_selected_ingest_profile.set(
              list(ingest_profiles_names_for_options.keys())[0])
      # pady tuple is (top, bottom)
      label_ingest_profile_instruction.grid(row=6, column=0, padx=padding)
      options_ingest_profiles = OptionMenu(
          root, options_selected_ingest_profile, *ingest_profiles_names_for_options.keys())
      options_ingest_profiles.grid(row=6, column=1)
      button_ingest_profile_select.grid(row=6, column=2)
      
      #Log
      write_custom_line("Got ingest profiles from Terentia server.")
    except Exception as error:
      # Log
      write_custom_line("INGEST FAILED")
      write_custom_line(error)
      print("INGEST FAILED")
      print(error)


def get_upload_ap_data():
    global upload_ap_data
    global upload_ap_container_name
    global upload_ap_account_name
    global upload_ap_key

    try:
      print("Getting the upload access provider data.")
      upload_ap_data = get_upload_access_provider()
      print(upload_ap_data)
      upload_ap_container_name = upload_ap_data[0]
      upload_ap_account_name = upload_ap_data[1]
      upload_ap_key = upload_ap_data[2]

      #Log
      write_custom_line("Got upload access provider from Terentia server.")
    except Exception as error:
      # Log
      write_custom_line("INGEST FAILED")
      write_custom_line(error)
      print("INGEST FAILED")
      print(error)


def select_ingest_profile():
    global selected_ingest_profile

    try:
      selected_ingest_profile = ingest_profiles_names_for_options[options_selected_ingest_profile.get(
      )]
      print("Selected ingest profile: ")
      print(selected_ingest_profile)
      box_response = messagebox.askokcancel("Ingest Profile Confirm", "You have selected the Ingest Profile: '" +
                                            options_selected_ingest_profile.get() + "' Are you sure you want to use this one?")
      if box_response == True:
          label_chosen_ingest_profile.config(
              text="3) Selected Ingest Profile: " + options_selected_ingest_profile.get())
          label_chosen_ingest_profile.grid(row=6, column=0, sticky="nw", padx=padding, pady=(padding, 0))
          label_ingest_profile_instruction.grid_remove()
          options_ingest_profiles.configure(state=DISABLED)
          button_ingest_profile_select.configure(state=DISABLED)
          label_select_folder_instruction.grid(row=7, column=0, sticky="nw", padx=padding)
          button_select_folder.grid(row=7, column=1, sticky="nw", padx=padding * 2)

          #Log
          write_custom_line(f"Ingest Profile Selected: {selected_ingest_profile}")
    except Exception as error:
      # Log
      write_custom_line("INGEST FAILED")
      write_custom_line(error)
      print("INGEST FAILED")
      print(error)


def select_folder():
    global folder_path

    try:
      folder_path = filedialog.askdirectory(
          initialdir=current_dir, title="Select your Migration Data Folder")
      label_selected_folder_path.grid(row=8, column=0, padx=padding, pady=(padding, 0))
      if folder_path == None or len(folder_path) == 0:
          label_selected_folder_path.config(text="Please select a folder path.")
      else:
          label_selected_folder_path.config(
              text="Selected folder path: " + folder_path)
          label_parse_status.grid(row=9, column=0, sticky="nw", padx=padding)
          label_parse_status.config(text="Parsing data. Please wait...")
          root.update()
          load_csv_and_assets()

          #Log
          write_custom_line(f"Folder selected: {folder_path}")
    except Exception as error:
      # Log
      write_custom_line("INGEST FAILED")
      write_custom_line(error)
      print("INGEST FAILED")
      print(error)

# Load the metadata and the assets and prep for processing.
# Starts off by checking if the .csv exists using csv_scripts search_directory func.


def load_csv_and_assets():
    global csv_file
    global asset_files
    global parsed_json_data
    global total_number_of_valid_records

    try:
      search_result = search_directory(folder_path)
      print(search_result)
      label_csv_file_status.grid(row=10, column=0, sticky="nw", padx=padding)
      label_asset_files_list_status.grid(row=11, column=0, sticky="nw", padx=padding * 2)
      if search_result["csv_file"] == None:
          write_custom_line(
              "Selected a folder that does not contain a .csv.", "INFO")
          print("No csv detected.")
          label_csv_file_status.config(
              text="No .csv file was detected. Please check your folder, ensure a .csv in it, then re-select it.")
      else:
          write_custom_line("Selected a folder with a .csv.", "INFO")
          write_custom_line(f".csv file: {search_result['csv_file']}")
          print("csv detected")
          csv_file = search_result["csv_file"]
          asset_files = search_result["asset_files"]
          label_csv_file_status.config(text="5) .csv name: " + csv_file)
          label_asset_files_list_status.config(
              text="Number of asset files detected: " + str(len(asset_files)))
          write_custom_line(
              f"Detected {str(len(asset_files))} asset files in the selected folder.", "INFO")
          parsed_json_data = csv_script(folder_path)

          # Validate metadata
          valid_records = parsed_json_data["valid_records"].copy()
          invalid_records = parsed_json_data["invalid_records"].copy()

          # - get metadata type ids
          res_get_ingest_profile = get_session_var().get(
            f"{vars_to_import.host_url}/ingest-profiles/{selected_ingest_profile}")
          ingest_profile = res_get_ingest_profile.json()
          form_id = ingest_profile["form_id"]
          res_get_form = get_session_var().get(
            f"{vars_to_import.host_url}/forms/{form_id}")
          metadata_type_ids = {}
          for metadata_type in res_get_form.json()["config"]["fields"]:
            name = metadata_type["label"]
            metadata_type_ids[name] = metadata_type["id"]

          new_valid_records = []
          for rec in valid_records:
            is_metadata_okay = check_metadata(metadata_type_ids, rec)
            if (is_metadata_okay[0] == True):
              new_valid_records.append(rec)
            else:
              invalid_records.append(rec)
              write_failure_line(rec["Asset Title"]["value"], is_metadata_okay[1])
          
          print(len(new_valid_records))

          print("Number of valid records: " +
                str(len(new_valid_records)))
          print("Number of invalid records: " +
                str(len(invalid_records)))
          # Case 1: No valid records, but at least 1 invalid record exists.
          if (new_valid_records == None or len(new_valid_records) == 0) and (invalid_records != None and len(invalid_records) > 0):
              print("Case 1: There are no valid records, but there are invalid records.")
              label_parse_status.config(
                  text="Detected at least 1 invalid record, but no valid records. There are no valid records in this .csv to parse.", bg="#FF0000")
          # Case 2: No valid records, no invalid records.
          elif new_valid_records == None or len(new_valid_records) == 0 or invalid_records == None or len(invalid_records) == 0:
              print("Case 2: There are no valid or invalid records.")
              label_parse_status.config(
                  text="Detected no valid records and no invalid records. It seems there is no parseable data in the .csv file.", bg="#FF0000")
          # Case 3: Good data.
          else:
              print("Case 3: Good data.")
              label_parse_status.config(
                  text="Data has been parsed.")
              label_valid_records.config(
                  text="Number of valid records: " + str(len(new_valid_records)), bg="#0bb310")
              total_number_of_valid_records = len(
                  new_valid_records)
              label_invalid_records.config(text="Number of invalid records: " + str(
                  len(invalid_records)), bg="#FFA500")
              label_valid_records.grid(row=12, column=0, sticky="nw", padx=padding * 2)
              label_invalid_records.grid(row=13, column=0, sticky="nw", padx=padding * 2)
              label_ingest_status.grid(row=14, column=0, sticky="nw", padx=padding)
              # Disable old inputs
              button_select_folder.configure(state=DISABLED)

              label_top_counter.config(text=f"Beginning Record Processing.")
              root.update()
              process_valid_records(new_valid_records)
    except Exception as error:
      # Log
      write_custom_line("INGEST FAILED")
      write_custom_line(error)
      print("INGEST FAILED")
      print(error)


def process_valid_records(valid_records):
    global path_of_current_asset_file
    global folder_path
    global total_number_of_valid_records

    # Reset to None
    current_record_number = 1
    path_of_current_asset_file = None

    print("Starting to process valid records.")
    for record in valid_records:
        print(
            f"Checking record {current_record_number} of {total_number_of_valid_records}")
        label_top_counter.config(
            text=f"Processing record {current_record_number} of {total_number_of_valid_records}")
        progress_number = (current_record_number /
                          total_number_of_valid_records) * 100
        progress_processed_assets["value"] = progress_number
        root.update()
        if record["Asset Title"]["value"] in asset_files:
            print("The record with the file name (aka Asset Title) of '" +
                  record["Asset Title"]["value"] + "' has a matching asset file.")
            # This is a valid file, so it should exist.
            path_of_current_asset_file = find_file_by_name(
                record["Asset Title"]["value"], folder_path)
            path_of_current_asset_file = os.path.normpath(
                path_of_current_asset_file)
            print("The path of the current asset is: ")
            print(path_of_current_asset_file)        

            process_a_record(record)
            write_success_line(record["Asset Title"]["value"])
        else:
            print("There was no matching asset file for a record with the file name of '" +
                  record["Asset Title"]["value"] + "'")
            write_failure_line(record["Asset Title"]["value"],
                              "There was no matching asset file for this record.")
        current_record_number = current_record_number + 1
        print("  ")
        print("  ")
    # This is the exit point of the application.
    # At this point all assets should have been processed.
    label_top_status_debug.config(text="Finished processing your assets.")
    label_ingest_status.config(text="6) Ingest complete. Please refer to log file")
    print("-------------------------------------------------------")
    print("Ingest Complete - Please refer to log file")
    print("-------------------------------------------------------")
    root.update()
    close_file_end_logger()


def process_a_record(record_data):
  print("Processing a record")
  formatted_record = {
    "name": record_data["Asset Title"]["value"],
    "type": record_data["Manual Asset Type"]["value"]
  }
  if ("Short Description" in record_data):
    formatted_record["short_description"] = record_data["Short Description"]["value"]
  if ("Long Description" in record_data):
    formatted_record["long_description"] = record_data["Long Description"]["value"]
  
  creation_response = create_records(
    user_id, selected_ingest_profile, formatted_record, record_data)
  uploadToAzure(upload_ap_data, path_of_current_asset_file,
              creation_response)

def find_file_by_name(name, path):
    for root, dirs, files, in os.walk(path):
        files_split_list = map(lambda file: os.path.splitext(file), files)
        files_split_dict = dict(files_split_list) # (filename, ext) into {filename: ext}
        return os.path.join(root, name + files_split_dict[name])


# Start logger
initialize_and_start_logger()

# Widget creations.
# Title
label_title = Label(
  root, text="Terentia - Client Asset Migration", font=50)
# Step 0: Host Entry
label_host_entry_instruction = Label(
    root, text="1) Enter the host URL of the Terentia you're using. Please use full URL and include the protocol.")
label_host_entry_instruction_2 = Label(
    root, text="(Good entry: 'https://MyTerentiaUrl.io'  Bad entry: 'MyTerentiaUrl.io')")
entry_host_url = Entry(root)
button_confirm_host = Button(root, text="Confirm Host", command=confirm_host)
label_host_status = Label(root, text="Awaiting Host...")
# Login
label_login_status = Label(root, text="2) Login Status: Awaiting login...")

label_username_entry = Label(root, text="Username:")
entry_username = Entry(root)

label_password_entry = Label(root, text="Password:")
entry_password = Entry(root, show="*")

button_login = Button(root, text="Login", command=attempt_login)
label_login_error = Label(root, bg="#ff1100", fg="#ffffff")


# Step 1: Ingest Profile Selection
label_ingest_profile_instruction = Label(
    root, text="3) Select the Ingest Profile you want to use for this migration.")
button_ingest_profile_select = Button(
    root, text="Okay", command=select_ingest_profile)
label_chosen_ingest_profile = Label(root, text="Waiting...")

# Step 2: Select folder with the data in it.
label_select_folder_instruction = Label(
    root, text="4) Select the folder containing your migration data (.csv), and your assets.")
button_select_folder = Button(
    root, text="Open Folder Selection", command=select_folder)
label_selected_folder_path = Label(root, text="Awaiting path.")
label_csv_file_status = Label(root, text="Awaiting csv results.")
label_asset_files_list_status = Label(root, text="Awaiting asset files.")

# Step 3: Parsing
label_parse_status = Label(root, text="Awaiting parse.")
label_valid_records = Label(root, text="Awating parse.")
label_invalid_records = Label(root, text="Awating parse.")

# Step 4: Creations and upload.
label_ingest_status = Label(root, text="6) Ingest in progress... Refer to Status Window")

# Widget placements.
label_title.grid(row=0, column=0, sticky="nw", padx=padding, pady=padding)
label_host_entry_instruction.grid(row=1, column=0, sticky="nw", padx=padding)
label_host_entry_instruction_2.grid(row=2, column=0, sticky="nw", padx=padding * 2)
entry_host_url.config(width=50)
entry_host_url.grid(row=3, column=0, sticky="nw", padx=padding * 2, pady=padding)
button_confirm_host.grid(row=4, column=0, sticky="nw", padx=padding * 2)

# Host entry was added after login
# We can just grid_remove() these, then re-add later
# once the user confirms the host.
# grid_remove() keeps the position saved so re-adding is easy.
label_login_status.grid(row=2, column=0,  sticky="nw", padx=padding)
label_login_status.grid_remove()
label_username_entry.grid(row=3, column=0, sticky="nw", padx=padding * 2)
label_username_entry.grid_remove()
entry_username.grid(row=3, column=1, sticky="nw", padx=padding * 2)
entry_username.grid_remove()
label_password_entry.grid(row=4, column=0, sticky="nw", padx=padding * 2)
label_password_entry.grid_remove()
entry_password.grid(row=4, column=1, sticky="nw", padx=padding * 2)
entry_password.grid_remove()

button_login.grid(row=5, column=0, sticky="nw", padx=padding * 2)
button_login.grid_remove()
label_login_error.grid(row=5, column=1)
# Hide the error message for now.
label_login_error.grid_remove()


# Root loop.
mainloop()
