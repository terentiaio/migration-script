from modules.feathers_api_calls import create_records, get_asset_class, login, get_ingest_profiles, get_upload_access_provider
from modules.csv_script import csv_script, json_logger
from modules.upload_to_azure import uploadToAzure

# From GUI
# res_login = login("admin@terentia.io", "Terentia1")
# print("logged in", res_login)

# file_ext = "pdf"
# asset_class = get_asset_class(file_ext)
# if asset_class == None:
#     raise("Unknown asset class!")

# asset_data = {
#     "name": "Some name",
#     "short_description": "This is the short description",
#     "long_description": "This is the long description",
#     "type": asset_class
# }

# create_records(res_login["user"]["id"],
#                "25809a1a-5534-479d-a310-a9a279ecda77", asset_data)

#######################################################################################################
# The script for parsing 'Sample Migration Data.csv' into Terentia database values.
# Filepath retrieved from GUI file dialog.

filepath = './Test Directory'
json_data = csv_script(filepath)

print("VALID RECORDS:")
json_logger(json_data["valid_records"])
print("\nINVALID RECORDS:")
json_logger(json_data["invalid_records"])
print("NUMBER OF VALID RECORDS:", len(json_data["valid_records"]))

################################################
# Azure testing example usage: the test variable grabs the list of AP config items that the Azure script needs to upload. This is then passed into the upload method directly
#                with the file path extracted above (hardcoded here for example purposes) and the version ID generated
# azureCreds = get_upload_access_provider()
# uploadToAzure(azureCreds, 'Test Directory\Copy of MOT-000100.jpg',
#               '47ea1c3b-1bd2-4ad6-9dc2-00c8d22b0d6a')
