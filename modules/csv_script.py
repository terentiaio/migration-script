import os
import json
import pandas as pd
from operator import itemgetter
from modules.reports_logger import initialize_and_start_logger, write_success_line, write_failure_line, close_file_end_logger

# Searches a directory using the given file path.
# Returns a dictionary containing the names of the CSV file (or None) and the asset files.
def search_directory(filepath):
    asset_files = []
    csv_file = None

    for file_name in os.listdir(filepath):
      if file_name.endswith('.csv'):
        csv_file = file_name
      else:
        asset_files.append(os.path.splitext(file_name)[0])

    files = {
        "csv_file": csv_file,
        "asset_files": asset_files
    }

    return files

# Parses CSV to JSON.
def csv_to_json(csv):
    df = pd.read_csv(csv, header=1)
    df.dropna(how="all", inplace=True)  # Remove empty rows.
    js = df.to_json(orient="records")
    json.loads(js)
    parsed = json.loads(js)
    
    mandatory = list(parsed[1].copy().values())

    # Another check for removing empty rows (None values).
    for file in parsed:
        if file["Asset Title"] == None:
            parsed.remove(file)

    csv_data = {
      "records": parsed[1:],
      "field_names": list(parsed[0].keys()),
      "metadata_types": list(parsed[0].values()),
      "mandatory": mandatory
    }

    return csv_data

# Log function
def json_logger(obj):
    print(json.dumps(obj, indent=4, sort_keys=True))

# The script for parsing 'Sample Migration Data.csv' into Terentia database values.
def csv_script(filepath):  
  # Throw error if the directory does not have a CSV file.
  if search_directory(filepath)["csv_file"] == None:
    raise FileNotFoundError('CSV file not found.')

  # Otherwise, parse the CSV file.
  else:
    directory_files = search_directory(filepath)

    csv_path = filepath + directory_files["csv_file"]
    if (not filepath.endswith("/")):
      csv_path = filepath + "/" + directory_files["csv_file"]

    # The complete set of JSON data converted from CSV file.    
    json_data = csv_to_json(csv_path)

    # JSON data is separated into categories.
    records = json_data["records"]
    field_names = json_data["field_names"]
    metadata_types = json_data["metadata_types"]
    mandatory = json_data["mandatory"]

    # Remove unnecessary columns which aren't part of the dataset (specific to sample data CSV).
    field_names.remove("Field names in Terentia")
    field_names.remove("Unnamed: 8")

    for record in records:
      record.pop("Field names in Terentia")
      record.pop("Unnamed: 8")

    invalid_records = []

    for file in records:
      # # Add file extension to asset title if it's missing.
      # if (os.path.splitext(file["Asset Title"])[1] == ""):
      #   file["Asset Title"] = file["Asset Title"] + "." + file["Manual Asset Type"]

      # Add mandatory and type of field information to the records.
      for (index, field_name) in enumerate(field_names):
        # Multi-select list values must be in a list.
        if metadata_types[index] == "multi-select":
          file[field_name] = {
            "value": file[field_name].replace(" ","").split(",") if file[field_name] else file[field_name],
            "type of field": metadata_types[index],
            "mandatory": mandatory[index]
          }
        else:
          file[field_name] = {
            "value": file[field_name].strip() if file[field_name] else file[field_name],
            "type of field": metadata_types[index],
            "mandatory": mandatory[index]
          }

    ### CHECK 1: FILE NOT FOUND
    # Make a copy for each loop so that the mutations won't affect the iterations of the original.
    records_copy = records.copy()

    for file in records_copy:
      # Check if the matching file exists in the directory.
      if (file["Asset Title"]["value"] not in directory_files["asset_files"]):
        invalid_records.append({
          "file": file,
          "reason": "File not found in directory."
        })
        records.remove(file)
        write_failure_line(file["Asset Title"]["value"], "File not found in directory.")

    ### CHECK 2: MANDATORY FIELD IS NULL
    # Make a copy for each loop so that the mutations won't affect the iterations of the original.
    records_copy = records.copy()

    for file in records_copy:
      # Check if mandatory fields are valid.

      for key in file.items():
        if (key[1]["mandatory"] == "Yes" and not key[1]["value"]):
          invalid_records.append({
            "file": file,
            "reason": "Mandatory field is null."
          })
          records.remove(file)
          write_failure_line(file["Asset Title"]["value"], f"Mandatory field '{key[0]}' is missing.")

    ### CHECK 3: SAME NAME RECORD ALREADY EXISTS
    # Make a copy for each loop so that the mutations won't affect the iterations of the original.
    records_copy = records.copy()

    for file in records_copy:
      # filtered_records = list(filter(lambda record: (record["Asset Title"]["value"] == file["Asset Title"]["value"]), records_copy))
      filtered_records = [record for record in records_copy if record["Asset Title"]["value"] == file["Asset Title"]["value"]]

      
      if (len(filtered_records) > 1):
        for record in filtered_records[1:]:
          invalid_records.append({
            "file": record,
            "reason": "Same name record already exists."
          })
          
          records.remove(record)
          records_copy.remove(record)
          write_failure_line(record["Asset Title"]["value"], f"'{record['Asset Title']['value']}' has the same name as another record.")

    records = {
      "valid_records": records,
      "invalid_records": invalid_records
    }

    return records