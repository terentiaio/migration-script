import os, uuid
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient, __version__, generate_account_sas, ResourceTypes, AccountSasPermissions, ContentSettings
import requests
from requests.auth import HTTPBasicAuth
import json
from datetime import datetime, timedelta
import mimetypes

def generateSAS(account, key):
  """This function generates a SAS token which is used for setting up the credentials to run Azure storage operations (in this particular case, uploading files as blobs to a container)

  Parameters:

  account (string): The Azure storage account name
  key (string): The access key for the Azure storage account

  Returns:

  string: The generated SAS token
  
  
  
  """
  return generate_account_sas(
    account_name=account, 
    account_key=key,
    resource_types=ResourceTypes(service=True, container=True, object=True),
    permission=AccountSasPermissions(read=True, write=True, add=True, create=True, delete=True, list=True),
    expiry=datetime.utcnow() + timedelta(weeks=999)
  )
def uploadToAzure(credentials, localFilePath, versionID):
  """ This function runs the upload process from a local file system to an Azure storage container (in this case, the uploads container).

  Steps:

  1) Make an API call to grab the access providers from the database to fetch the AP that connects to the 'uploads' container on Azure
  2) Create the SAS token with the account credentials inside of the AP config
  3) Create the necessary Azure clients for operations (BlobServiceClient -> ContainerClient -> BlobClient)
  4) Attempt to upload the file as a data chunk to an Azure blob using the BlobClient and the versionID param as the blob name

  Parameters:

    credentials (list): The credentials needed to perform Azure operations on containers/blobs (container name, storage account name, access key)

    localFilePath (string): The path to the local media file that needs to be uploaded to Azure

    versionID (string): The version UUID to set as the blob name so that the Azure functions can trigger as needed
  
  
  
  """
  containerName = credentials[0]
  accountName = credentials[1]
  accountKey = credentials[2]

  cred = generateSAS(accountName, accountKey)
  print(cred)
  service = BlobServiceClient(account_url=f"https://{accountName}.blob.core.windows.net", credential=cred)
  containerClient = service.get_container_client(containerName)
  ext = localFilePath.split('.')[-1]
  print("Determining file extension: " + ext)
  blobName = versionID + '.' + ext
  print("Creating blob name: " + blobName)
  blob = containerClient.get_blob_client(blobName)
  contentType = mimetypes.MimeTypes().guess_type(localFilePath)[0]
  print("Getting content type to set on blob: " + contentType)
  content_settings = ContentSettings(content_type=contentType)
  if not blob.exists():
    try:
      with open(localFilePath, 'rb') as data:
        blob.upload_blob(data, overwrite=True, content_settings=content_settings)
      print("Successfully uploaded blob to Azure storage")
    except:
      raise FileExistsError("Failed to upload file to Azure storage!")
  else:
    print("Blob already found on Azure, cancelling upload")