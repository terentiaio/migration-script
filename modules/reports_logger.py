# Imports
import time
import os

# Define variables
current_log_file_name = None
new_log_file = None
total_assets_processed = None
total_successful_assets = None
total_failed_assets = None

# Flushes the buffer so that the file write happens instantly instead of when file.close() is called
# - https://stackoverflow.com/questions/54134177/python-write-to-file-in-real-time
def flush():
  global new_log_file
  new_log_file.flush()

# Start the logger. Creates /Reports dir if needed.
# Creates a file with timestamp in the /Reports dir.


def initialize_and_start_logger():
    global new_log_file, total_assets_processed, total_successful_assets, total_failed_assets
    total_assets_processed = 0
    total_successful_assets = 0
    total_failed_assets = 0
    TAG = "@@@ reports_logger.py - initalize_and_start_logger(), "
    print(TAG + " is starting.")
    if os.path.isdir("Reports"):
        print(TAG + "Reports is dir.")
    else:
        print(TAG + "Reports not dir. Creating Reports folder.")
        os.mkdir("./Reports")

    # year month day - hour minute second
    current_date_timestamp = time.strftime("%Y-%m-%d - %H-%M-%S")
    print(TAG + "current_date_timestamp: " + current_date_timestamp)

    full_file_name = f"Log File - Ran at {current_date_timestamp}.txt"
    print(TAG + "full_file_name: " + full_file_name)
    new_log_file = open(f"./Reports/{full_file_name}", "w")
    new_log_file.write("START: Log file has been initialized.\n\n\n")
    flush()



# Write a success line to the file. Success lines means the asset has processed and uploaded.
# (asset_name) - The name of the asset that has been successfully ingested.
# (additional_message) - An additional message, if for some reason one must be shown.
def write_success_line(asset_name, additional_message=None):
    global new_log_file, total_assets_processed, total_successful_assets
    total_assets_processed += 1
    total_successful_assets += 1
    TAG = "@@@ reports_logger.py - write_success_line(), "
    print(TAG + "creating and writing a success line.")
    print(TAG + "asset_name: " + asset_name)
    if additional_message != None:
        print(TAG + "There is an additional_message: " + additional_message)

    new_log_file.write(f"SUCCESS: Processed and ingested '{asset_name}'\n")
    if additional_message != None:
        new_log_file.write(f"INFO: {additional_message}\n\n")
    else:
        new_log_file.write("\n")
    flush()


# Write a failure line to the file. Failure line means the asset has been rejected for whatever reason.
# (asset_name) - The name of asset that failed.
# (reason) - Mandatory additional info, the reason the file failed.
def write_failure_line(asset_name, reason):
    global new_log_file, total_assets_processed, total_failed_assets
    total_assets_processed += 1
    total_failed_assets += 1
    TAG = "@@@ reports_logger.py - write_failure_line(), "
    print(TAG + "creating and writing a failure line.")

    new_log_file.write(
        f"FAILURE: The asset '{asset_name}' failed to process and ingest.\n")
    new_log_file.write(f"INFO: {reason}\n\n")
    flush()


# Write a custom line if you need to.
# (message) - The message to write.
# (prepend) - Optional prepend.
def write_custom_line(message, prepend=None):
    global new_log_file
    TAG = "@@@ reports_logger.py - write_custom_line(), "
    if prepend != None:
        print(TAG + "writing a custom line with a prepend.")
        new_log_file.write(f"{prepend}: {message}\n\n")
    else:
        print(TAG + "writing a custom line with no prepend.")
        new_log_file.write(f"{message}\n\n")
    flush()


def close_file_end_logger():
    global new_log_file, total_assets_processed, total_successful_assets, total_failed_assets
    new_log_file.write(
        "\n\nEND: Reached end of program.\n")
    new_log_file.write(
        f"Processed a total of {total_assets_processed} records.\n")
    new_log_file.write(f"Successful assets: {total_successful_assets}.\n")
    new_log_file.write(f"Failed assets: {total_failed_assets}.\n")
    flush()
    new_log_file.close()


# Debug logger simulation.
# initialize_and_start_logger()

# write_success_line("Fake Asset 1")
# write_success_line(
#     "Fake Asset 2", "Fake asset had an Expanding List element that wasn't in. We added it to the list.")
# write_success_line('Fake asset 3')
# write_custom_line("Half way done!")
# write_failure_line(
#     "fake asset 4", "Could not find the file associated with this asset.")
# write_success_line("fake asset 6")
# write_custom_line("All done!", "CUSTOM PREPEND")


# close_file_end_logger()
