import requests
import json
import vars_to_import
from uuid import uuid4

# for storing session -- use this to make requests after logging in (session.post() / session.get())
session = None

def get_session_var():
  global session
  
  return session

# Logs into feathers server and returns the login object (including accessToken)
#   @param email
#   @param password
#   @return {dict} feathers login dict


def login(email, password):
    global session

    data_login = {
        "email": email,
        "password": password,
        "strategy": "local"
    }
    res_login = requests.post(
        f"{vars_to_import.host_url}/authentication", data_login)
    res_login_pretty = res_login.json()

    if "accessToken" in res_login_pretty:
        print("Good login, accessToken in res.")
        session = requests.Session()
        headers = {"Authorization": "Bearer " + res_login.json()["accessToken"],
                   "Content-type": "application/json"}
        session.headers.update(headers)
        formatted_response = {"status": "success", "data": res_login.json()}
        return formatted_response
    elif "message" in res_login_pretty:
        print("No accessToken in login. Error message returned.")
        formatted_response = {
            "status": "failedWithMessage", "data": res_login.json()}
        return formatted_response
    else:
        print("No accessToken in login, and no error message.")
        formatted_response = {
            "status": "failedNoMessage", "data": res_login.json()}
        return formatted_response


# Creates all the records that the Upload Modal (in the UI) creates (asset, metadata, version, ingest)
#   @param user_id
#   @param ingest_profile_id
#   @param asset_data { name, short_description, long_description, type }
#   @param file_ext
#   @return {string(UUID)} id of the created version


def create_records(user_id, ingest_profile_id, asset_data, metadata_records):
    global session
    data_cr_asset = asset_data

    # x) Get ingest_profile
    res_get_ingest_profile = session.get(
        f"{vars_to_import.host_url}/ingest-profiles/{ingest_profile_id}")
    ingest_profile = res_get_ingest_profile.json()
    folder_id = ingest_profile["folder_id"]
    watch_folder_id = ingest_profile["watch_folder_id"]
    form_id = ingest_profile["form_id"]

    # x) Get default security properties
    res_get_def_sec_props = session.get(
        f"{vars_to_import.host_url}/ingest-profile-controller?entity=default_security_properties&ingest_profile_id={ingest_profile_id}")
    def_sec_props = []
    for dsp in res_get_def_sec_props.json()["data"]:
        default_sec_prop = {
            "team_id": dsp["default_security_property.team_id"],
            "role_id": dsp["default_security_property.role_id"],
            "user_id": dsp["default_security_property.user_id"],
            "rule": dsp["default_security_property.rule"],
        }
        def_sec_props.append(default_sec_prop)

    # x) Create asset record
    data_cr_asset["folder_id"] = folder_id
    data_cr_asset["properties"] = {}
    data_cr_asset["properties"]["security"] = def_sec_props
    res_cr_asset = session.post(
        f"{vars_to_import.host_url}/asset_controller", data=json.dumps(data_cr_asset).encode('utf-8'))
    print("@@@@@@@@ got res_cur_asset response successful.")
    asset_id = res_cr_asset.json()["data"]["id"]

    # x) Get form record
    res_get_form = session.get(
        f"{vars_to_import.host_url}/forms/{form_id}")
    metadata_type_ids = {}
    for metadata_type in res_get_form.json()["config"]["fields"]:
        name = metadata_type["label"]
        metadata_type_ids[name] = metadata_type["id"]

    # x) Create metadata records
    #     - asset_id
    #     - metadata fields
    metadata_cr_data = {
        "asset_metadata": []
    }
    for key, value in metadata_records.items():
        if (key != "Asset Title" and key != "Short Description" and key != "Long Description"):
            record = {
                "asset_id": asset_id,
                "metadata_type_id": metadata_type_ids[key],
                "value": value["value"]
            }
            metadata_cr_data["asset_metadata"].append(record)
    res_cr_metadata = session.post(
        f"{vars_to_import.host_url}/metadata-controller", data=json.dumps(metadata_cr_data).encode('utf-8'))
    print("@@@@@@@@ res_cr_metadata response successful.")

    # x) Create version record
    data_cr_version = {
        "versions": {
            "id": str(uuid4()),
            "asset_id": asset_id,
        }
    }
    res_cr_version = session.post(
        f"{vars_to_import.host_url}/version-controller", data=json.dumps(data_cr_version).encode('utf-8'))
    version_id = res_cr_version.json()["data"]["versions"][0]["id"]
    print("@@@@@@@@ res_cr_version response successful.")
    # print("@@@@@@@@ res_cr_version response: ")
    # print(res_cr_version.json())

    # x) Create ingest record
    data_cr_ingest = {
        "version_id": version_id,
        "ingest_profile_id": ingest_profile_id,
        "watch_folder_id": watch_folder_id,
        "user_id": user_id
    }
    res_cr_ingest = session.post(
        f"{vars_to_import.host_url}/ingests", data=json.dumps(data_cr_ingest).encode('utf-8'))
    print("@@@@@@@@ res_cr_ingest response. Not outputting it to keep console clea.")
    # print("@@@@@@@@ res_cr_ingest response: ")
    # print(res_cr_ingest.json())

    return version_id

# Gets the asset class of the file extension
#   @param file_ext no period!
#   @return {string} Video/Image/Document/etc.


def get_asset_class(file_ext):
    global session

    asset_map_dict_id = "b4ac6c0d-b7a9-4fcf-b290-7e510398f80d"
    res_get_asset_map = session.get(
        f"{vars_to_import.host_url}/dictionary-controller/{asset_map_dict_id}?entity=dictionary")
    asset_class = None
    for map in res_get_asset_map.json()["config"]["items"]:
        for key in map:
            if key == file_ext:
                asset_class = map[key]
                break
    return asset_class

# Checks metadata records to make sure they are valid
# (really, as of August 20, 2021, it just checks to make sure that any values for lists are actually
#   members of the metadata_list in terentia)
def check_metadata(metadata_type_ids, record_data):
  global session
  
  metadata_records = {
    "metadata_records": []
  }
  for key, value in record_data.items():
    if (key != "Asset Title"):
        record = {
            "metadata_type_id": metadata_type_ids[key],
            "value": value["value"]
        }
        metadata_records["metadata_records"].append(record)

  res_cr_metadata = session.put(
    f"{vars_to_import.host_url}/metadata-controller/value-validation", data=json.dumps(metadata_records).encode('utf-8'))
  
  if (res_cr_metadata.json()["data"]["flagged"] == False):
    return (True, {})
  else:
    return (False, res_cr_metadata.json()["data"]["metadata_records"])

# Gets a list of the ingest profiles in the system
#   @return {list}


def get_ingest_profiles():
    global session

    res_get_ingest_profiles = session.get(
        f"{vars_to_import.host_url}/ingest-profiles/")
    return res_get_ingest_profiles.json()


def get_upload_access_provider():
    global session
    ''' This function gets a list of access providers in the system, filters them down to the 'uploads' container AP and returns the necessary values from it
   in order to use as Azure credentials

  Returns:

  A list of credentials (azure container name, storage account name and access key)


'''

    res_get_access_providers = session.get(
        f"{vars_to_import.host_url}/access-providers/")
    result = res_get_access_providers.json()
    aps = result['data']
    # Isolate the uploads AP
    configs = []
    accountName = ""
    accountKey = ""
    containerName = ""
    for i in aps:
        config = i['config']
        # print(config)
        configs.append(config)

    for i in configs:
        if ('containerName' in i):
            if (i['containerName'] == 'uploads'):
                containerName = i['containerName']
                accountName = i['accountName']
                accountKey = i['accountKey']

    print(
        f'Container name: {containerName}\nAccount name: {accountName}\nAccount key: {accountKey}')
    return [containerName, accountName, accountKey]
