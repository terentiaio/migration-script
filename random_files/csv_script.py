import os
import json
import pandas as pd    
from operator import itemgetter

# Searches a directory using the given file path.
# Returns a dictionary containing the names of the CSV file (or None) and the asset files.
def search_directory(filepath):
  asset_files = []
  csv_file = None

  for file_name in os.listdir(filepath):
    if file_name.endswith('.csv'):
      csv_file = file_name
    else:
      asset_files.append(file_name)
  
  files = {
    "csv_file": csv_file,
    "asset_files": asset_files
  }
  
  return files

# Parses CSV to JSON.
def csv_to_json(csv):
  df = pd.read_csv(csv)
  df.dropna(how="all", inplace=True) # Remove empty rows.
  js = df.to_json(orient="records")
  json.loads(js)
  parsed = json.loads(js)

  # Another check for removing empty rows (None values).
  for file in parsed:
    if file["Asset ID"] == None:
      parsed.remove(file)

  return parsed
      
def json_logger(obj):
  print(json.dumps(obj, indent=4, sort_keys=True))

def csv_script(filepath):
  # Throw error if the directory does not have a CSV file.
  if search_directory(filepath)["csv_file"] == None:
    raise FileNotFoundError('CSV file not found.')

  # Otherwise, parse the CSV file.
  else:
    directory_files = search_directory(filepath)
    csv_path = directory_files["csv_file"]
        
    json_data = csv_to_json(csv_path)
    invalid_records = []

    # Parse file extensions, if needed.
    for file in json_data:
      # Replace field names.
      # Note: this code is specifically made for the CSV file 'Sample Migration Data'.
      file["Title"] = file.pop("Asset Title")
      file["Asset Title"] = file.pop("Asset ID")
      file["Color"] = file.pop("Colour")
      file["Days Available"] = file.pop("Days of the Week")
      file["Author Name"] = file.pop("Author")
      file["Manual Asset Type"] = file.pop("Asset Type")
      # Remove unnecessary columns which aren't part of the dataset.
      file.pop("Fields names in the CSV")
      file.pop("Unnamed: 8")

      # Add file extension to asset title if it's missing.
      if (os.path.splitext(file["Asset Title"])[1] == ""):
        file["Asset Title"] = file["Asset Title"] + "." + file["Manual Asset Type"]

      # Add mandatory and type of field information to the records.
      file["Asset Title"] = {
        "value": file["Asset Title"],
        "type of field": "FCC - Title",
        "mandatory": "true"
      }
      file["Title"] = {
        "value": file["Title"],
        "type of field": "Free Text",
        "mandatory": "true"
      }
      file["Color"] = {
        "value": file["Color"],
        "type of field": "Static List",
        "mandatory": "true"
      }
      file["Category"] = {
        "value": file["Category"],
        "type of field": "Expanding List",
        "mandatory": "false"
      }
      file["Days Available"] = {
        "value": file["Days Available"],
        "type of field": "Multi-select List",
        "mandatory": "true"
      }
      file["Author Name"] = {
        "value": file["Author Name"],
        "type of field": "Free Text",
        "mandatory": "true"
      }
      file["Manual Asset Type"] = {
        "value": file["Manual Asset Type"],
        "type of field": "Free Text",
        "mandatory": "false"
      }

    # Make a copy for each loop so that the mutations won't affect the iterations of the original.
    json_data_copy = json_data.copy()

    for file in json_data_copy:
      # Check if the matching file exists in the directory.
      if (file["Asset Title"]["value"] not in directory_files["asset_files"]):
        invalid_records.append(file)
        json_data.remove(file)

    # Make a copy for each loop so that the mutations won't affect the iterations of the original.
    json_data_copy = json_data.copy()
    
    for file in json_data_copy:
      # Check if mandatory fields are valid.
      for key in file.items():
        if (key[1]["mandatory"] == "true" and not key[1]["value"]):
          print("check")
          invalid_records.append(file)
          json_data.remove(file)

    json_data = {
      "valid_records": json_data,
      "invalid_records": invalid_records
    }

    print("VALID RECORDS:")
    json_logger(json_data["valid_records"])
    print("\nINVALID RECORDS:")
    json_logger(json_data["invalid_records"])

##############################################################
filepath = "."  # Retrieved from GUI file dialog.
csv_script(filepath)